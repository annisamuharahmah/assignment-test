﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TestWebApp.Models;
using TestWebApi.Model;
using Newtonsoft.Json;
using System.Text;

namespace TestWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly string apiUrl;
        private readonly HttpClient httpClient = new HttpClient();

        public HomeController(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
        }
        private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public async Task<IActionResult> Index()
        {
            List<Buku> book = new List<Buku>();
            
            book = JsonConvert.DeserializeObject<List<Buku>>(await httpClient.GetStringAsync(apiUrl + "Home"));
                
           return View(book);            
        }

        public async Task<IActionResult> Add()
        {
            Buku book = new Buku();           

            return View(book);
        }

        [HttpPost]
        public async Task<IActionResult> Add(Buku book)
        {
            Buku data = new Buku();
            
                string jsonData = JsonConvert.SerializeObject(book);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                data = JsonConvert.DeserializeObject<Buku>(await
                    (await httpClient.PostAsync(apiUrl + "Home/Create", content)
                    ).Content.ReadAsStringAsync());

            ViewBag.action = "ditambahkan";

            return View("success");
        }

        public async Task<IActionResult> Edit(int id)
        {
                Buku? book = JsonConvert.DeserializeObject<Buku>(
                    await httpClient.GetStringAsync(apiUrl + "Home/" + id));

                return View(book);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Buku book)
        {
            Buku? data = new Buku();
            string jsonData = JsonConvert.SerializeObject(book);
            HttpContent? content = new StringContent(jsonData, Encoding.UTF8, "application/json");
            data = JsonConvert.DeserializeObject<Buku>(await
                    (await httpClient.PutAsync(apiUrl + "Home/Update", content)
                    ).Content.ReadAsStringAsync());

            ViewBag.action = "diedit";

            return View("success");
        }

        public IActionResult Hapus(int id)
        {
            return View(id);
        }

        [HttpPost]
        public async Task<IActionResult> Hapus(Buku data)
        {
            Buku? book = new Buku();

            string jsonData = JsonConvert.SerializeObject(data);
            HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

            book = JsonConvert.DeserializeObject<Buku>(await
                (await httpClient.DeleteAsync(apiUrl + "Home/Delete/" + data.Id)).Content.ReadAsStringAsync());

            ViewBag.action = "dihapus";
            return View("success");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}