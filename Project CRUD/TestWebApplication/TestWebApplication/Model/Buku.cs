﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TestWebApi.Model
{
    [Table("buku")]
    public partial class Buku
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("judul")]
        [StringLength(30)]
        [Unicode(false)]
        public string Judul { get; set; } = null!;
        [Column("pengarang")]
        [StringLength(50)]
        [Unicode(false)]
        public string Pengarang { get; set; } = null!;
        [Column("penerbit")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Penerbit { get; set; }
        [Column("tanggaTerbit", TypeName = "datetime")]
        public DateTime? TanggaTerbit { get; set; }
        [Column("created_by")]
        public long CreatedBy { get; set; }
        [Column("created_on", TypeName = "datetime")]
        public DateTime CreatedOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("is_availble")]
        [StringLength(1)]
        [Unicode(false)]
        public string IsAvailble { get; set; } = null!;
        [Column("is_delete")]
        [StringLength(1)]
        [Unicode(false)]
        public string IsDelete { get; set; } = null!;
    }
}
