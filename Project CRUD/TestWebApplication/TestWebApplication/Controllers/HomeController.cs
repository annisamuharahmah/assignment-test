﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestWebApi.Model;

namespace TestWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        private readonly dbperpustakaanContext db;
        public HomeController(dbperpustakaanContext _db)
        {
            db = _db;
        }

        [HttpGet()]
        public List<Buku> GetAll()
        {
            List<Buku> data = (
                from b in db.Bukus
                where b.IsDelete == "0"
                select new Buku
                {
                    Id = b.Id,
                    Judul = b.Judul,
                    Pengarang = b.Pengarang,
                    Penerbit = b.Penerbit,
                    TanggaTerbit = b.TanggaTerbit,

                    CreatedBy = b.CreatedBy,
                    CreatedOn = b.CreatedOn,
                    ModifiedBy = b.ModifiedBy,
                    ModifiedOn = b.ModifiedOn,

                    IsAvailble = b.IsAvailble,
                    IsDelete = b.IsDelete,
                }
                ).ToList();
            return data;
        }

        [HttpGet("{id}")]
        public Buku GetId(int Id)
        {
            Buku data = (
                from b in db.Bukus
                where b.IsDelete == "0" && b.Id == Id
                select new Buku
                {
                    Id = b.Id,
                    Judul = b.Judul,
                    Pengarang = b.Pengarang,
                    Penerbit = b.Penerbit,
                    TanggaTerbit = b.TanggaTerbit,

                    CreatedBy = b.CreatedBy,
                    CreatedOn = b.CreatedOn,
                    ModifiedBy = b.ModifiedBy,
                    ModifiedOn = b.ModifiedOn,

                    IsAvailble = b.IsAvailble,
                    IsDelete = b.IsDelete,
                }
                ).FirstOrDefault();
            return data;
        }

        [HttpPost("[action]")]
        public Buku Create(Buku buku)
        {
            Buku data = new Buku();

            data.Judul = buku.Judul;
            data.Pengarang = buku.Pengarang;
            data.Penerbit = buku.Penerbit;
            data.TanggaTerbit = buku.TanggaTerbit;

            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;

            data.IsAvailble = "1";
            data.IsDelete = "0";

            db.Add(data);
            db.SaveChanges();

            return db.Bukus.Where(x => x.Id == data.Id).FirstOrDefault();
        }

        [HttpPut("[action]")]
        public Buku Update(Buku buku)
        {
            Buku data = (
                from b in db.Bukus
                where b.Id == buku.Id
                select new Buku
                {
                    Id = b.Id,
                    CreatedBy = b.CreatedBy,
                    CreatedOn = b.CreatedOn,
                    IsAvailble = b.IsAvailble,
                    IsDelete = b.IsDelete,
                }
                ).FirstOrDefault();

            data.Judul = buku.Judul;
            data.Pengarang = buku.Pengarang;
            data.Penerbit = buku.Penerbit;
            data.TanggaTerbit = buku.TanggaTerbit;

            data.ModifiedBy = 1;
            data.ModifiedOn = DateTime.Now;

            db.Update(data);
            db.SaveChanges();

            return data;
        }

        [HttpDelete("[action]/{id}")]
        public Buku Delete(int Id)
        {
            Buku data = (
                from b in db.Bukus
                where b.Id == Id
                select new Buku
                {
                    Id = b.Id,
                    CreatedBy = b.CreatedBy,
                    CreatedOn = b.CreatedOn,
                    Judul = b.Judul,
                    Pengarang = b.Pengarang,
                    Penerbit = b.Penerbit,
                    TanggaTerbit = b.TanggaTerbit,

                    IsAvailble = b.IsAvailble,
                }
                ).FirstOrDefault();

            data.ModifiedBy = 1;
            data.ModifiedOn = DateTime.Now;
            data.IsDelete = "1";

            db.Update(data);
            db.SaveChanges();            

            return data;
        }

    }
}
