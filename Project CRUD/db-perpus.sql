USE [master]
GO
/****** Object:  Database [dbperpustakaan]    Script Date: 19/12/2023 14:05:46 ******/
CREATE DATABASE [dbperpustakaan]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbperpustakaan', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\dbperpustakaan.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbperpustakaan_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\dbperpustakaan_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [dbperpustakaan] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbperpustakaan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbperpustakaan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbperpustakaan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbperpustakaan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbperpustakaan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbperpustakaan] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbperpustakaan] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbperpustakaan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbperpustakaan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbperpustakaan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbperpustakaan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbperpustakaan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbperpustakaan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbperpustakaan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbperpustakaan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbperpustakaan] SET  ENABLE_BROKER 
GO
ALTER DATABASE [dbperpustakaan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbperpustakaan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbperpustakaan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbperpustakaan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbperpustakaan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbperpustakaan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbperpustakaan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbperpustakaan] SET RECOVERY FULL 
GO
ALTER DATABASE [dbperpustakaan] SET  MULTI_USER 
GO
ALTER DATABASE [dbperpustakaan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbperpustakaan] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbperpustakaan] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbperpustakaan] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbperpustakaan] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbperpustakaan] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'dbperpustakaan', N'ON'
GO
ALTER DATABASE [dbperpustakaan] SET QUERY_STORE = ON
GO
ALTER DATABASE [dbperpustakaan] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [dbperpustakaan]
GO
/****** Object:  Table [dbo].[buku]    Script Date: 19/12/2023 14:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[buku](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[judul] [varchar](30) NOT NULL,
	[pengarang] [varchar](50) NOT NULL,
	[penerbit] [varchar](20) NULL,
	[tanggaTerbit] [datetime] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[is_availble] [varchar](1) NOT NULL,
	[is_delete] [varchar](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[buku] ON 

INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (1, N'Buku Kita Semua', N'Aan', N'Penerbit Surya', CAST(N'2020-08-17T00:00:00.000' AS DateTime), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-18T15:21:03.110' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (2, N'Gelas Bening', N'Cerminan Jiwa', N'Penerbit Surya', CAST(N'2020-12-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-18T14:55:29.770' AS DateTime), NULL, NULL, N'1', N'0')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (3, N'Cara memelihara Kucing Update', N'Pet meow', N'Animalia lovers', CAST(N'2020-09-18T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-18T14:59:23.950' AS DateTime), 1, CAST(N'2023-12-18T15:14:10.040' AS DateTime), N'1', N'0')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (4, N'AAA', N'bbb', N'mons', NULL, 1, CAST(N'2023-12-19T11:08:56.907' AS DateTime), 1, CAST(N'2023-12-19T13:58:34.053' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (5, N'abc', N'uncle ong', N'Sunrise wonderland', CAST(N'2021-11-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-19T11:15:23.927' AS DateTime), 1, CAST(N'2023-12-19T12:33:42.103' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (6, N'jam tangan', N'-', N'mons', NULL, 1, CAST(N'2023-12-19T11:18:03.240' AS DateTime), 1, CAST(N'2023-12-19T12:25:54.083' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (7, N'test', N'tester', N'tes', CAST(N'2012-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-19T12:28:16.380' AS DateTime), 1, CAST(N'2023-12-19T12:28:33.580' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (8, N'tas merah', N'adudu', N'kapal layar ent.', CAST(N'2021-12-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-19T12:39:27.370' AS DateTime), 1, CAST(N'2023-12-19T12:39:36.697' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (9, N'asal-usul bumi', N'earthone', N'Bumi Langit ', NULL, 1, CAST(N'2023-12-19T12:55:36.910' AS DateTime), 1, CAST(N'2023-12-19T13:56:31.803' AS DateTime), N'1', N'0')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (10, N'hujan', N'air', N'a', CAST(N'2021-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-19T12:57:25.163' AS DateTime), 1, CAST(N'2023-12-19T12:57:34.773' AS DateTime), N'1', N'1')
INSERT [dbo].[buku] ([id], [judul], [pengarang], [penerbit], [tanggaTerbit], [created_by], [created_on], [modified_by], [modified_on], [is_availble], [is_delete]) VALUES (11, N'Gajah dan Kawan-Kawan', N'Zootupia', N'Animalia lovers', CAST(N'2021-04-04T00:00:00.000' AS DateTime), 1, CAST(N'2023-12-19T13:57:58.887' AS DateTime), 1, CAST(N'2023-12-19T13:58:16.430' AS DateTime), N'1', N'0')
SET IDENTITY_INSERT [dbo].[buku] OFF
GO
ALTER TABLE [dbo].[buku] ADD  DEFAULT ((0)) FOR [is_availble]
GO
ALTER TABLE [dbo].[buku] ADD  DEFAULT ((0)) FOR [is_delete]
GO
USE [master]
GO
ALTER DATABASE [dbperpustakaan] SET  READ_WRITE 
GO
