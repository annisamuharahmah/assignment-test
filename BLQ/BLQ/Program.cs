﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal_2();
            //soal_3();
            //soal_4();
            //soal_5();
            //soal_6();
            //soal_7();
            //soal_8();
            //soal_9();
            //soal_10();
            //soal_11();
            //soal_12();
            //soal_13();
            //soal_14();
            //soal_15();
            //soal_16();
            //soal_17();
            //soal_18();
            //soal_19();
            //soal_22();
        }

        static void soal_1()
        {
            int uang = 1000;
            int[] kaca_mata = {0, 500, 600, 700, 800};
            int[] baju = {0, 200, 400, 350};
            int[] sepatu = {0, 400, 350, 200, 300 };
            int[] buku = {0, 100, 50, 150};
            var total = new List<int>();
            var murah = new List<int>();

            foreach (int i in kaca_mata)
            {
                foreach (int j in baju)
                {
                    foreach (int k in sepatu)
                    {
                        foreach (int l in buku)
                        {
                            total.Add(i+j+k+l);
                        }
                    }
                }
            }

            foreach (int i in total)
            {
                if (total[i] <= uang)
                    murah.Add(total[i]);
            }

            murah.Sort();
            var item = new List<int>();
            int m = murah.Count - 1;
            int kc = murah[m];

            Console.WriteLine($"Jumlah item yang bisa dibeli ");
        }

        static void soal_2()
        {
            string format = "d MMMM yyyy";
            Console.Write("Masukan tanggal pinjam (e.g. 27 Januari 2019)");
            string time1 = Console.ReadLine();
            DateTime masuk = DateTime.ParseExact(time1, format, new System.Globalization.CultureInfo("id-ID"));

            Console.Write("Masukan tanggal kembali (e.g. 27 Januari 2019)");
            string time2 = Console.ReadLine();
            DateTime keluar = DateTime.ParseExact(time2, format, new System.Globalization.CultureInfo("id-ID"));

            //DateTime masuk1 = new DateTime(masuk.Year, masuk.Month, masuk.Day, masuk.Hour, masuk.Minute, masuk.Second);
            //DateTime keluar1 = new DateTime(keluar.Year, keluar.Month, keluar.Day, keluar.Hour, keluar.Minute, keluar.Second);

            TimeSpan lama = keluar - masuk;

            int dendaA = lama.Days - 14;
            int dendaB = lama.Days - 3;
            int dendaC = lama.Days - 7;
            int dendaD = lama.Days - 7;

            if (dendaA > 0)
            {
                dendaA = dendaA*100;
            }
            else
            {
                dendaA = 0;
            }

            if (dendaB > 0)
            {
                dendaB = dendaB * 100;
            }
            else
            {
                dendaB = 0;
            }

            if (dendaC > 0)
            {
                dendaC = dendaC * 100;
            }
            else
            {
                dendaC = 0;
            }

            if (dendaD > 0)
            {
                dendaD = dendaD * 100;
            }
            else
            {
                dendaD = 0;
            }

            Console.WriteLine("Denda buku A : Rp " + dendaA);
            Console.WriteLine("Denda buku B : Rp " + dendaB);
            Console.WriteLine("Denda buku C : Rp " + dendaC);
            Console.WriteLine("Denda buku D : Rp " + dendaD);

        }

        static void soal_3()
        {
            string format = "dd MMMM yyyy | HH:mm:ss";
            Console.Write("Masukan tanggal dan jam masuk (e.g. 27 Januari 2019 | 05:00:01)");
            string time1 = Console.ReadLine();
            DateTime masuk = DateTime.ParseExact(time1,format, new System.Globalization.CultureInfo("id-ID"));

            Console.Write("Masukan tanggal jam keluar (e.g. 27 Januari 2019 | 05:00:01)");
            string time2 = Console.ReadLine();
            DateTime keluar = DateTime.ParseExact(time2, format, new System.Globalization.CultureInfo("id-ID"));

            //DateTime masuk1 = new DateTime(masuk.Year, masuk.Month, masuk.Day, masuk.Hour, masuk.Minute, masuk.Second);
            //DateTime keluar1 = new DateTime(keluar.Year, keluar.Month, keluar.Day, keluar.Hour, keluar.Minute, keluar.Second);
            
            TimeSpan lama = keluar - masuk;
            int bayar = 0;
            if (lama.Hours <= 8)
            {
                bayar = (lama.Hours + 1) * 1000;
            }
            else if (lama.Hours <= 24)
            {
                bayar = 8000;
            }
            else
            {
                bayar = 15000 + ((24 - lama.Hours)*1000);
            }
            Console.WriteLine("Total Jam : " + lama.Hours);
            Console.WriteLine("Total Bayar : Rp " + bayar);
        }

        static void soal_4()
        {
            //Console.Write("Masukan angka awal ");
            int angkaAwal = 2;
            Console.Write("Masukan angka n ");
            int n = int.Parse(Console.ReadLine());
            var primenumber = new List<int>();
            int angkaAkhir = 100;
            int l = 0;
            for (int i = angkaAwal; i < angkaAkhir; i++)
            {
                int tampung = 0;
                for (int j = 1; j <= angkaAkhir; j++)
                {
                    if (i % j == 0)
                    {
                        tampung++;
                    }
                }
                if (tampung < 3)
                {
                    primenumber.Add(i);                        
                }

                if (primenumber.Count == n)
                {
                    break;
                }
            }

            Console.WriteLine(string.Join(' ',primenumber));
            //Console.WriteLine("Angka yang termasuk bilangan prima dari {0} sampai {1}", angkaAwal, angkaAkhir);
            //for (int k = 0; k < primenumber.Count; k++)
            //{
            //    Console.Write(primenumber[k]);
            //    Console.Write(' ');
            //}
        }

        static void soal_5()
        {
            Console.WriteLine("Masukan n");
            int n = Convert.ToInt32(Console.ReadLine());

            List<int> fibonacci = new List<int>();
            fibonacci.Add(1); fibonacci.Add(1);

            for (int i = 0; i < n - 2; i++)
            {
                fibonacci.Add(fibonacci[i] + fibonacci[i + 1]);
            }

            Console.WriteLine("fibonacci : " + string.Join(' ', fibonacci));

        }

        static void soal_6()
        {
            Console.WriteLine("Masukan kata");
            string kalimat = Console.ReadLine();

            bool palindrome = true;

            for (int i = 0; i < kalimat.Length/2; i++)
            {
                if (kalimat[i] != kalimat[kalimat.Length - 1 - i])
                {
                    palindrome = false;
                }
            }

            if (palindrome == true) Console.WriteLine("Kata ini palindrome");
            else Console.WriteLine("Kata ini bukan palindrome");

        }
        static void soal_7()
        {
            int[] number = { 8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3 };
            Console.WriteLine(string.Join(' ', number));

            Array.Sort(number);
            double mean = (double)(number.Sum()/number.Length);

            int mode = number.GroupBy(v => v).OrderByDescending(g => g.Count()).First().Key;

            Console.WriteLine(string.Join(' ', number));

            Console.WriteLine("Mean : " + mean);
            Console.WriteLine("Median : " + number[number.Length/2]);
            Console.WriteLine("Modus : " + mode);

        }

        static void soal_8()
        {
            int[] num = { 1, 2, 4, 7, 8, 6, 9 };
            Console.WriteLine(string.Join(' ',num));
            Array.Sort(num);
            int min = num[0] + num[1] + num[3] + num[4];

            Array.Reverse(num);
            int max = num[0] + num[1] + num[3] + num[4];

            Console.WriteLine("nilai minimal dan maksimal dari penjumlahan 4 komponen deret adalah : " + min + " dan " + max);
        }

        static void soal_9()
        {
            Console.WriteLine("Masukan N");
            int n = Convert.ToInt32(Console.ReadLine());

            List<int> number = new List<int>();

            for (int i = 0; i < n; i++)
            {
                number.Add(n*(i+1));
            }

            Console.WriteLine($"N = {n} -> " + string.Join(' ', number));
        }

        static void soal_10()
        {
            Console.WriteLine("Masukan kalimat");
            string kalimat = Console.ReadLine();

            string kalimatBaru = "";

            string[] kataKata = kalimat.Split(' ');

            foreach (string kata in kataKata)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (i == 0)
                    {
                        kalimatBaru = kalimatBaru + kata.Substring(i, 1);
                    }
                    else if (i == 5 - 1)
                    {
                        int length = kata.Length - 1;
                        kalimatBaru = kalimatBaru + kata.Substring(length, 1);
                    }
                    else
                    {
                        kalimatBaru = kalimatBaru + "*";
                    }

                }
                kalimatBaru = kalimatBaru + " ";
            }
            Console.WriteLine(kalimatBaru);
        }

        static void soal_11()
        {
            Console.Write("Masukan kata ");
            string input = Console.ReadLine();
            char[] chrArray = input.ToCharArray();

            string vokal = "aiueoAIUEO";
            int star = 0;
            foreach (char c in input)
            {
                if (vokal.Contains(c))
                {
                    star++;
                }
            }

            Array.Reverse(chrArray);
            for (int i = 0; i < chrArray.Count(); i++)
            {
                Console.WriteLine($"{chrArray[i]}".PadLeft(star+1, '*').PadRight(star*2+1, '*'));
            }
        }

        static void soal_12()
        {
            List<int> num = new List<int> { 1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8 };

            for (int i = 0; i<num.Count; i++)
            {
                for (int j=0; j<num.Count; j++)
                {
                    if (num[i] < num[j])
                    {
                        int x = num[i];
                        num[i] = num[j];
                        num[j] = x;
                    }
                }
            }

            Console.WriteLine(string.Join(' ', num));
        }

        static void soal_13()
        {
            Console.Write("Masukan jam (eg. 3:00)");
            DateTime waktu = DateTime.ParseExact(Console.ReadLine(), "h:mm", new System.Globalization.CultureInfo("id-ID"));

            double jam = waktu.Hour;
            double menit = waktu.Minute;
            double jarumPanjang = jam * 30 + ((menit / 60) * 30);
            double jarumPendek = menit * 6;

            double jarak = Math.Abs(jarumPanjang-jarumPendek);

            if (jarak > 180) jarak = 360-jarak;
            Console.WriteLine(jarak);
            //int derajat = jarak * 6;
 
        }

        static void soal_14()
        {
            Console.WriteLine("masukan n integer");
            int n = int.Parse(Console.ReadLine());


            Console.WriteLine("masukan deret");
            string input = Console.ReadLine();

            List<string> arr = input.Split(' ').ToList();
            int x = arr.Count;
            if (n > x) n = n % 26;
            
            for (int i = 0; i<n; i++)
            {
                arr.Add(arr[i]);
            }

            arr.RemoveRange(0,n);            

            string output = string.Join(' ', arr);
            Console.WriteLine(output);

        }

        static void soal_15()
        {
            Console.WriteLine("Ubah format waktu \"03:40:44 PM\" menjadi format 24 jam");
            string time12 = "03:40:44 PM";
            DateTime time = DateTime.ParseExact(time12, "hh:mm:ss tt", null);

            Console.WriteLine(time.ToString("HH:mm:ss"));
        }

        static void soal_16()
        {
            List<int> menu = new List<int> { 42, 50, 30, 70, 30};
            int person = 4;
            int total = menu.Sum() - menu[0];

            double price1 = total / person;
            double price2 = (total / person) + (menu[0]/3);

            Console.WriteLine("Harga menu-menu : " + string.Join((' '), menu));
            Console.WriteLine("Jumlah yang dibayar orang yang alergi ikan : " + price1);
            Console.WriteLine("Jumlah yang dibayar orang yang tidak alergi ikan : " + price2);
        }

        static void soal_17()
        {
            Console.WriteLine("Masukan jalur");
            string input = Console.ReadLine();

            int memory2 = 0;
            int memory1 = 0;

            int count = 0;
            int lembah = 0;
            int gunung = 0;

            foreach (char i in input)
            {
                if (i == 'N') count++;
                else if (i == 'T') count--;

                memory2 = memory1;
                memory1 = count;

                if (memory1 == 0 && memory2 == -1)
                {
                    lembah++;
                }
                else if (memory1 == 0 && memory2 == 1)
                {
                    gunung++;
                }
                
                Console.WriteLine(memory1);
            }

            Console.WriteLine($"Hattori melewati gunung {gunung} dan {lembah} lembah");
        }

        static void soal_18()
        {
            double jamOl = 0.1 * (9 * 30 + 13 * 20 + 15 * 50 + 17 + 80);
            
            Console.Write("Jumlah air yang diminum per 30 menit : ");
            int minumPer30Menit = 100;

            Console.Write("Jumlah air yang diminum setelah olahraga : ");
            int lastMinum = 500;

            double totalMinum = (((jamOl*2-1)*minumPer30Menit)+lastMinum)/1000;

            Console.WriteLine("Total air yang dimimum Donna : " + (int)totalMinum + "liter");

        }

        static void soal_19()
        {
            Console.WriteLine("Masukan kalimat");
            string input = Console.ReadLine().ToLower();

            string huruf = "abcdefghijklmnopqrstuvwxyz";

            foreach (char c in input)
            {
                if (huruf.Contains(c))
                {
                    huruf = huruf.Replace(c,'0');
                }
            }

            int count = huruf.Where(x => x == '0').Count();

            if (count < 26) Console.WriteLine("Kalimat bukan pangram");
            else Console.WriteLine("Kalimat pangra");
        }

        static void soal_20()
        {
            Console.Write("Jarak awal : ");
            int jarak = int.Parse(Console.ReadLine());
            
            Console.Write("Pola A : ");
            string A = Console.ReadLine();
            char[] chrA = A.ToCharArray();
            
            Console.Write("Pola B : ");
            string B = Console.ReadLine();
            char[] chrB = B.ToCharArray();

            int stepA = 0;
            int stepB = jarak;
            string win = "";
            
            for (int i = 0; i<chrA.Length; i++)
            {
                if (chrA[i] == 'B')
                {
                    if (chrB[i] == 'G')
                    {
                        stepA += 2;
                        stepB -= 1;
                    }
                    else if (chrB[i] == 'K')
                    {
                        stepB += 2;
                        stepA -= 1;
                    }
                }

                if (chrA[i] == 'G')
                {
                    if (chrB[i] == 'K')
                    {
                        stepA += 2;
                        stepB -= 1;
                    }
                    else if (chrB[i] == 'B')
                    {
                        stepB += 2;
                        stepA -= 1;
                    }
                }

                if (chrA[i] == 'K')
                {
                    if (chrB[i] == 'B')
                    {
                        stepA += 2;
                        stepB -= 1;
                    }
                    else if (chrB[i] == 'G')
                    {
                        stepB += 2;
                        stepA -= 1;
                    }
                }                
            }

        }

        static void soal_22()
        {
            int[] lilin = { 3, 3, 9, 6, 7, 8, 23};
            int[] detik = { 1, 1, 2, 3, 5, 8, 13};
            int leleh = 0;
            int s = 1;

            while (s > 0)
            {
                for (int l = 0; l<lilin.Count(); l++)
                {
                    lilin[l] = lilin[l] - detik[l];

                    if (lilin[l] <= 0)
                    {
                        leleh = l;
                        break;
                    }
                }
                if (leleh != 0)
                {
                    break;
                }
            }
            Console.WriteLine($"lilin ke-{leleh + 1}");


        }
    }
}
